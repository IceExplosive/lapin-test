use std::time::Duration;

use async_amqp::*;
use async_std::task::sleep;
use lapin::{Connection, ConnectionProperties};

mod logger;

#[async_std::main]
async fn main() {
    logger::init();
    let mut conn = try_connect().await;

    loop {
        println!("Connected: {}", conn.status().connected());
        if !conn.status().connected() {
            conn = try_connect().await;
        }

        sleep(Duration::from_secs(2)).await;
    }
}

async fn try_connect() -> Connection {
    loop {
        let conn_call = Connection::connect(
            "amqp://127.0.0.20:5672",
            ConnectionProperties::default().with_async_std(),
        );

        if let Ok(conn) = conn_call.await {
            println!("Successfully Connected");

            return conn;
        }

        println!("Retry in 2 seconds");
        sleep(Duration::from_secs(2)).await;
    }
}
