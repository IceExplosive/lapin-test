use log::{set_logger, set_max_level, Log, Metadata, Record, LevelFilter};

static LOGGER: Logger = Logger;

pub struct Logger;

pub fn init() {
    set_logger(&LOGGER).unwrap();

    set_max_level(LevelFilter::Info);
}

impl Log for Logger {
    fn enabled(&self, _meta: &Metadata) -> bool {
        true
    }

    fn log(&self, record: &Record) {
        println!("{} {}:{} {}", record.level(), record.file().unwrap_or(""), record.line().unwrap_or(0), record.args());
    }

    fn flush(&self) {}
}
