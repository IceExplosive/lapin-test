init:
	docker-compose up -d && cargo run

restart:
	docker-compose restart rabbitmq
